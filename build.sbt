name := "whoto"

version := "1.3-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
   jdbc,
   filters,
   "org.webjars"  %% "webjars-play"    % "2.4.0-1",
   "org.webjars"  %  "jquery"          % "2.1.4",
   "org.webjars"  %  "bootstrap"       % "3.3.6",
   "com.typesafe.play.modules" %% "play-modules-redis" % "2.4.1",
   "org.sedis"    %% "sedis"           % "1.2.2",
   "org.mindrot"  %  "jbcrypt"         % "0.3m",
   "jp.t2v"       %% "play2-auth"      % "0.14.1",
   "jp.t2v"       %% "play2-auth-test" % "0.14.1" % Test,
  "org.mockito"   % "mockito-core"     % "1.10.19" % "test",
   specs2 % Test
//   ,
//   play.sbt.Play.autoImport.cache
)

resolvers += "google-sedis-fix" at "http://pk11-scratch.googlecode.com/svn/trunk"

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

routesGenerator := InjectedRoutesGenerator
