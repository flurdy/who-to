package controllers

import play.api.http.HttpErrorHandler
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import play.api.mvc.Results._
import scala.concurrent._

class ErrorHandler(implicit val messagesApi: MessagesApi) extends HttpErrorHandler with Controller with I18nSupport {

  def onClientError(request: RequestHeader, statusCode: Int, message: String) = {
    implicit val flash: Flash = request.flash
    Future.successful(
      Status(statusCode)(views.html.error.client(statusCode,message))
    )
  }

  def onServerError(request: RequestHeader, exception: Throwable) = {
    implicit val flash: Flash = request.flash
    Future.successful(
      InternalServerError(views.html.error.internalserver())
    )
  }
}
