package models

import play.api._
import play.api.Play.current
import org.mindrot.jbcrypt.BCrypt
import lifecycle.WhoComponents


trait UserCredentialsEntity extends RedisEntity {
	def user: ApplicationUser
   lazy val id: Option[UserId] = user.userId
   lazy val redisRootKey  = id.map( userId => s"credentials:userId:$userId" )
   lazy val permissionKey = redisParameterKey("permission")
   lazy val passwordKey   = redisParameterKey("password")
}

trait ApplicationUserRepositoryEntity {
	val MaxIdKey   = "applicationUser:maxUserIds"
	val UserIdsKey = "applicationUser:userIds"
}

trait ApplicationUserEntity extends RedisEntity with ApplicationUserRepositoryEntity {
	def username: String
	def userId: Option[UserId]
   lazy val id            = userId
   lazy val redisRootKey  = id.map( userId => s"applicationUser:userId:$userId" )
   lazy val userIdKey     = s"applicationUser:username:$username:userId"
   lazy val usernameKey   = redisParameterKey(s"username")
   lazy val fullnameKey   = redisParameterKey(s"fullname")
   lazy val emailKey      = redisParameterKey(s"email")
   lazy val whoIdsKey	  = redisParameterKey("who.whoIds")
}

case class ApplicationUser(userId: Option[UserId], username: String, email: Option[String], fullname: Option[String])
					(implicit applicationUserRepository: ApplicationUserRepository,
						whoRepository: WhoRepository, sedis: SedisWrapper) extends ApplicationUserEntity {

	val search = new SearchEntity {
		val id = userId
	}

	def this(username: String, email: Option[String], fullname: Option[String])
			   (implicit applicationUserRepository: ApplicationUserRepository, whoRepository: WhoRepository, sedis: SedisWrapper) =
			this(None,username,email,fullname)

	def updateEmail() = updateParameter(emailKey,email)

	def updateFullname() = updateParameter(fullnameKey,fullname)

	private def updateParameter(wantedKey: Option[String], value: Option[String]) = {
		wantedKey.map( key => value.fold[Any]( sedis.del(key) )( sedis.set(key, _) ) )
		this
	}


	def delete() {
		Logger.info(s"Deleting application user $username")
		userId.map{ userId =>
			whoRepository.deleteAll(this)
			sedis.del(userIdKey)
			usernameKey.map( sedis.del )
			fullnameKey.map( sedis.del )
			emailKey.map(    sedis.del )
			sedis.srem(UserIdsKey, userId)
		}
	}
	def register(): ApplicationUser = {
		Logger.info(s"Registering user $username")
		userId.fold[ApplicationUser]{
				val newUserId = sedis.incr(MaxIdKey).toString
				sedis.sadd(UserIdsKey, newUserId)
				this.copy(userId = Some(newUserId)).register
			}{ newUserId =>
				sedis.set(userIdKey, newUserId)
				usernameKey.map( key => sedis.set(key, username) )
				fullnameKey.map( key => fullname.map( value => sedis.set(key, value) ) )
				emailKey.map   ( key => email.map   ( value => sedis.set(key, value) ) )
				this
			}
	}
}

case class UserCredentials(user: ApplicationUser, password: Option[String], permission: Option[Permission])
					(implicit userCredentialsRepository: UserCredentialsRepository, sedis: SedisWrapper) extends UserCredentialsEntity {

	def this(user: ApplicationUser)(implicit userCredentialsRepository: UserCredentialsRepository, sedis: SedisWrapper) = this(user,None,None)

	def register(): UserCredentials = {
		user.userId.fold[UserCredentials]{
				this.copy(user = user.register).register
			}{ userId =>
				Logger.info(s"Registering credentials for ${user.username}")
				permissionKey.map( key => sedis.set(key, NormalUser.toString) )
				passwordKey.map  ( key => password.map( value => sedis.set(key, encrypt(value)) ) )
				this
			}
	}

	def changePassword() = {
		Logger.info(s"Changing password for ${user.username}")
		for{
			userId      <- user.userId
			password    <- password
		 	passwordKey <- passwordKey
		} yield sedis.set(passwordKey, encrypt(password))
		this
	}

	private def encrypt(password: String): String = BCrypt.hashpw(password,BCrypt.gensalt())

	def delete() = {
		Logger.info(s"Deleting credentials for ${user.username}")
		user.delete
		passwordKey.map  ( sedis.del )
		permissionKey.map( sedis.del )
	}
}


sealed trait Permission
case object Administrator extends Permission
case object NormalUser    extends Permission
case object Anyone        extends Permission

object Permission {

  def valueOf(value: String): Permission = value match {
    case "Administrator" => Administrator
    case "NormalUser"    => NormalUser
    case "Anyone"        => Anyone
    case _ => throw new IllegalArgumentException()
  }

}


class UserCredentialsRepositoryComponent(whoComponents: WhoComponents) extends UserCredentialsRepository {
	implicit val applicationUserRepository = whoComponents.applicationUserRepository
	implicit val sedis = whoComponents.sedis
}

trait UserCredentialsRepository {

	implicit val applicationUserRepository: ApplicationUserRepository
	implicit def sedis: SedisWrapper
	implicit val userCredentialsRepository: UserCredentialsRepository = this

	def findById(userId: String)(implicit sedis: SedisWrapper): Option[UserCredentials] = applicationUserRepository.findById(userId) flatMap ( findByApplicationUser(_) )

	def findByApplicationUser(applicationUser: ApplicationUser): Option[UserCredentials] = {
		for {
			userId         <- applicationUser.userId
			userCredentialsEntity = new UserCredentialsEntity { val user = applicationUser }
			permissionKey  <- userCredentialsEntity.permissionKey
			permissionName <- sedis.get(permissionKey)
			permission: Permission = Permission.valueOf(permissionName)
		} yield UserCredentials(applicationUser, None, Some(permission))
	}


	def authenticate(username: String, enteredPassword: String): Option[UserCredentials] = {
		for {
			usernameUser    <- applicationUserRepository.findByUsername(username)
			hashedPassword  <- findCredentials(usernameUser)
			if BCrypt.checkpw(enteredPassword, hashedPassword)
			userCredentials <- findByApplicationUser(usernameUser)
		} yield userCredentials
	}


	private def findCredentials(applicationUser: ApplicationUser): Option[String] = {
		for {
			userId <- applicationUser.userId
			userCredentialsEntity = new UserCredentialsEntity { val user = applicationUser }
			passwordKey <- userCredentialsEntity.passwordKey
			password    <- sedis.get(passwordKey)
		} yield password
	}
}


class ApplicationUserRepositoryComponent(whoComponents: WhoComponents) extends ApplicationUserRepository {
	implicit val whoRepository = whoComponents.whoRepository
	implicit val sedis = whoComponents.sedis
}

trait ApplicationUserRepository {

	implicit def whoRepository: WhoRepository
	implicit def sedis: SedisWrapper
	implicit val applicationUserRepository: ApplicationUserRepository = this

	val UsernameBlacklist = Set("username","support","admin","root","owner","helpdesk","help","account","info","test","sysadmin")

	def isUsernameAlreadyRegistered(username: String): Boolean = findByUsername(username.trim.toLowerCase).isDefined

	def isUsernameBlacklisted(username: String): Boolean = UsernameBlacklist.contains(username.trim.toLowerCase) || isUsernameStemBlacklisted(username.trim.toLowerCase)

	def isUsernameStemBlacklisted(usernameStem: String): Boolean = {
		val blacklisted = UsernameBlacklist.contains(usernameStem.trim.toLowerCase)
		if( blacklisted || usernameStem.size <= 2) blacklisted
		else isUsernameStemBlacklisted(usernameStem.dropRight(1))
	}

	def isUsernameAvailable(username: String): Boolean = !isUsernameBlacklisted(username.trim.toLowerCase) && !isUsernameAlreadyRegistered(username.trim.toLowerCase)

	def isUsernameValid(username: String): Boolean = username != null && username.trim.matches("""^[a-zA-Z0-9_\-\.@\+]{3,128}$""")

	def isUsernameValidAndAvailable(username: String): Boolean = isUsernameValid(username.trim.toLowerCase) && isUsernameAvailable(username.trim.toLowerCase)

	def findByUsername(usernameToLookFor: String): Option[ApplicationUser] = {
		val keyEntity = new ApplicationUserEntity{
			val username = usernameToLookFor
			val userId = None
		}
		sedis.get(keyEntity.userIdKey) map { userId =>
			toApplicationUser(userId, usernameToLookFor)
		}
	}

	private def toApplicationUser(userIdToLookFor: UserId, usernameToLookFor: String): ApplicationUser = {
		val keyEntity = new ApplicationUserEntity{
			val username = usernameToLookFor
			val userId = Some(userIdToLookFor)
		}
		val email    = keyEntity.emailKey.flatMap( sedis.get )
		val fullname = keyEntity.fullnameKey.flatMap( sedis.get )
		ApplicationUser(Some(userIdToLookFor),usernameToLookFor,email,fullname)
	}

	def findById(userIdToLookFor: UserId): Option[ApplicationUser] = {
		val keyEntity = new ApplicationUserEntity {
			val username = ""
			val userId = Some(userIdToLookFor)
		}
		for {
			key      <- keyEntity.usernameKey
			username <- sedis.get(key)
		} yield toApplicationUser(userIdToLookFor,username)
	}
}
