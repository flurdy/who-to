package models

import play.api._
import lifecycle.WhoComponents

object Gender extends Enumeration {
  type Gender = Value
  val Female  = Value("Female")
  val Male    = Value("Male")
}

object Relationship extends Enumeration {
  type Relationship = Value
  val Married =  Value("Married")
  val Engaged =  Value("Engaged")
  val Divorced = Value("Divorced")
  val Widow =    Value("Widow")
  val Partner =  Value("Partner")
  val Friend =   Value("Girl or Boyfriend")
  val Ex =       Value("Ex")
  val Complicated = Value("Complicated")
}

import models.Gender._
import models.Relationship._


trait WhoChildPartnerEntity extends RedisEntity {
   lazy val nameKey    = redisParameterKey("name")
   lazy val notesKey   = redisParameterKey("notes")
   lazy val genderKey  = redisParameterKey("gender")
}

trait ChildEntity extends WhoChildPartnerEntity with ChildRepositoryEntity {
   lazy val redisRootKey = id.map( childId => s"child:childId:$childId" )
   lazy val whoIdKey     = redisParameterKey("who:whoId")
   lazy val birthYearKey = redisParameterKey("birthYear")
}

trait PartnerEntity extends WhoChildPartnerEntity with PartnerRepositoryEntity {
   lazy val redisRootKey    = id.map( partnerId => s"partner:partnerId:$partnerId" )
   lazy val whoIdKey        = redisParameterKey("who:whoId")
   lazy val relationshipKey = redisParameterKey("relationship")
}

trait WhoRepositoryEntity {
   val MaxWhoIdKey = "who:maxWhoId"
}

trait PartnerRepositoryEntity {
   val MaxPartnerIdKey = "partner:maxPartnerId"
}

trait ChildRepositoryEntity {
   val MaxChildIdKey = "child:maxChildId"
}

trait WhoEntity extends WhoChildPartnerEntity with WhoRepositoryEntity {
   lazy val redisRootKey   = id.map( whoId => s"who:whoId:$whoId" )
   lazy val ownerIdKey     = redisParameterKey("owner:ownerId")
   lazy val partnerIdsKey  = redisParameterKey("partner:partnerIds")
   lazy val childrenIdsKey = redisParameterKey("child:childrenIds")
}

case class Child(who: Who, id: Option[ChildId], name :Option[String], birthYear: Option[Int],
                  notes: Option[String], gender: Gender)
                (implicit sedis: SedisWrapper) extends ChildEntity {

   def isChildOf(thatWho: Who): Boolean = {
      val isChild = for {
         thatWhoId       <- thatWho.id
         thisWhoId       <- thatWho.id
            if thatWhoId == thisWhoId
         childId         <- id
         childWhoIdKey   <- whoIdKey
         childWhoId      <- sedis.get(childWhoIdKey)
            if childWhoId == thatWhoId
         childIdsKey     <- thatWho.childrenIdsKey
      } yield sedis.sismember(childIdsKey,childId)
      isChild.contains(true)
   }

   def delete = {
      nameKey.map(   sedis.del )
      notesKey.map(  sedis.del )
      genderKey.map( sedis.del )
      whoIdKey.map(  sedis.del )
      who.childrenIdsKey.map( key => id.map( childId => sedis.srem(key,childId) ) )
   }


   def update = {
      genderKey.map( sedis.set(_, gender.toString) )
      nameKey.map( key      => name.fold[Any](      sedis.del(key) )( name =>      sedis.set( key, name) ) )
      birthYearKey.map( key => birthYear.fold[Any]( sedis.del(key) )( birthYear => sedis.set( key, s"$birthYear") ))
      notesKey.map( key     => notes.fold[Any](     sedis.del(key) )( notes =>     sedis.set( key, notes) ) )
      this
   }


  def add(who: Who) = {
    who.id.map{ whoId =>
      val childId = sedis.incr(MaxChildIdKey).toString
      Logger.info(s"adding child $childId")
      who.childrenIdsKey.map ( sedis.sadd(_, childId) )
      val newChild = this.copy( id = Some(childId) )
      newChild.whoIdKey.map(     key => sedis.set(key,whoId) )
      newChild.nameKey.map(      key => name map ( name => sedis.set(key, name) ) )
      newChild.genderKey.map(    key => sedis.set(key, gender.toString) )
      newChild.birthYearKey.map( key => birthYear.map( birthYear => sedis.set(key, s"$birthYear") ) )
      newChild.notesKey.map(     key => notes.map( notes => sedis.set(key, notes) ) )

      newChild
    }
  }

}

case class Partner(who: Who, id: Option[PartnerId], name: String,
                   relationship: Option[String], notes: Option[String], gender: Gender)
                  (implicit sedis: SedisWrapper) extends PartnerEntity {

   def isPartnerOf(thatWho: Who): Boolean = {
     val isPartner = for {
         thatWhoId       <- thatWho.id
         thisWhoId       <- thatWho.id
            if thatWhoId == thisWhoId
         partnerId       <- id
         partnerWhoIdKey <- whoIdKey
         partnerWhoId    <- sedis.get(partnerWhoIdKey)
            if thatWhoId == partnerWhoId
         partnerIdsKey   <- thatWho.partnerIdsKey
      } yield sedis.sismember(partnerIdsKey,partnerId)
      isPartner.contains(true)
   }


   def delete = {
     nameKey.map( sedis.del )
     genderKey.map( sedis.del )
     notesKey.map( sedis.del )
     whoIdKey.map( sedis.del )
     who.partnerIdsKey.map( partnerIdsKey => id.map( sedis.srem(partnerIdsKey,_) ) )
   }


   def update = {
      nameKey.map( sedis.set(_, name) )
      notesKey.map(        key => notes.fold[Any]( sedis.del(key) )( sedis.set(key, _) ) )
      relationshipKey.map( key => relationship.fold[Any]( sedis.del(key) )( sedis.set(key, _) ) )
      genderKey.map(       key => sedis.set(key, gender.toString ) )
      this
   }


  def add(who: Who): Option[Partner] = {
    who.id.map { whoId =>
      val partnerId = sedis.incr(MaxPartnerIdKey).toString
      Logger.info(s"adding partner $partnerId")
      val newPartner = this.copy( id = Some(partnerId) )
      who.partnerIdsKey.map( sedis.sadd(_, partnerId) )
      newPartner.whoIdKey.map( sedis.set(_,whoId) )
      newPartner.nameKey.map( sedis.set(_, name) )
      newPartner.notesKey.map( key => notes.map( sedis.set(key, _) ) )
      newPartner.genderKey.map( sedis.set(_, gender.toString) )
      newPartner.relationshipKey.map( key => relationship.map( sedis.set(key, _) ) )

      newPartner
    }
  }
}

case class Who(owner: Option[ApplicationUser], id: Option[WhoId], name: String,
               partners: Set[Partner], children: Set[Child], notes: Option[String], gender: Option[Gender])
             (implicit whoRepository: WhoRepository, partnerRepository: PartnerRepository,
              childRepository: ChildRepository, sedis: SedisWrapper, searchWho: SearchWho) extends WhoEntity {

  def this(id: Option[WhoId], name: String)
         (implicit whoRepository: WhoRepository, partnerRepository: PartnerRepository,
                   childRepository: ChildRepository, sedis: SedisWrapper, searchWho: SearchWho) =
      this( None, id, name, Set.empty, Set.empty, None, None)

  def this(owner: ApplicationUser, id: Option[WhoId], name: String)
         (implicit whoRepository: WhoRepository, partnerRepository: PartnerRepository,
                   childRepository: ChildRepository, sedis: SedisWrapper, searchWho: SearchWho) =
      this( Some(owner), id, name, Set.empty, Set.empty, None, None)

   private def isOwner(thatOwner: ApplicationUser) = owner.contains(thatOwner)

  def save(owner: ApplicationUser): Option[Who] = if(id.isEmpty) add(owner)
                                                  else update(owner)

  def add(owner: ApplicationUser): Option[Who] = {
    Logger.info(s"${owner.username} is adding $name to their contact list")
    for {
      ownerId    <- owner.userId
      whoId      =  sedis.incr(MaxWhoIdKey).toString
      newWho     =  this.copy( id = Some(whoId), owner = Some(owner) )
    }  yield {
      owner.whoIdsKey.map( sedis.sadd(_, whoId) )
      newWho.ownerIdKey.map( sedis.set(_, ownerId) )
      newWho.nameKey.map( sedis.set(_, name) )
      newWho.notesKey.map( notesKey => notes.map( sedis.set(notesKey, _) ) )
      newWho.genderKey.map( gKey => gender.map( gender => sedis.set(gKey, gender.toString) ) )
      searchWho.addNamesToSearchRegistry(owner,whoId,name)
      newWho
    }
  }

  def update(owner: ApplicationUser): Option[Who] = {
      isOwnedBy(owner).some.map{ _ =>
         notesKey.map(  key => notes.fold [Any](sedis.del(key))( sedis.set(key, _ ) ))
         genderKey.map( key => gender.fold[Any](sedis.del(key))( gender => sedis.set(key, gender.toString ) ))
         nameKey.map {  key =>
            sedis.get(key).map{ oldName =>
               id.map { whoId =>
                  sedis.set(key, name )
                  searchWho.updateNamesInSearchRegistry(owner, whoId, oldName, name)
               }
            }
        }
        this
     }
   }

  def updateName(owner: ApplicationUser, editName: String) = this.copy(name = editName).update(owner)

  def updateNotes(owner: ApplicationUser, newNotes: String) = this.copy(notes = Some(newNotes)).update(owner)

  def populateWithPartners(): Who = {
    val partnersFound = partnerRepository.findPartnersOfWho(this)
    this.copy(partners = partnersFound)
  }

  def populateWithChildren(): Who = {
    val childrenFound = childRepository.findChildrenOfWho(this)
    this.copy(children = childrenFound)
  }

  def delete(owner: ApplicationUser) = {
    if(isOwnedBy(owner)) {
       Logger.info(s"Deleting who ${id}")
       id.map{ whoId =>
          searchWho.removeNamesFromSearchRegistry(owner, whoId, name)
          owner.whoIdsKey.map( sedis.srem(_,whoId) )
       }
       removeAllPartners
       removeAllChildren
       nameKey.map ( sedis.del )
       notesKey.map ( sedis.del )
       genderKey.map ( sedis.del )
       ownerIdKey.map ( sedis.del )
    } else {
      throw new IllegalAccessException("NOT YOUR WHO")
    }
  }


   def addPartner(partner: Partner): Who = {
      partner.add(this).fold[Who](this)( newPartner => this.copy(partners = partners + newPartner) )
   }

  def findPartnerById(partnerId: String): Option[Partner] = partnerRepository.findById( this, partnerId)

  def removePartner(partner: Partner) = {
    partner.id.fold(this){ partnerId =>
      partner.isPartnerOf(this).fold(this){ _ =>
        partner.delete
        this.copy(partners = partners - partner )
      }
    }
  }

  def removeAllPartners = {
    partners.foreach( removePartner )
    this.copy(partners = Set.empty)
  }

   def updatePartner(partner: Partner): Partner = {
      partner.isPartnerOf(this).fold[Partner](partner)(_.update)
   }

  def findChildById(childId: String): Option[Child] = childRepository.findById( this, childId)

   def addChild(child: Child): Who = {
      child.add(this).fold[Who](this)( newChild => this.copy(children = children + newChild) )
   }

   def removeChild(child: Child) = {
      child.isChildOf(this).fold(this){ _ =>
         child.delete
         this.copy(children = children.filter(_.id != child.id))
      }
   }

  def removeAllChildren = {
     children.foreach( removeChild )
     this.copy(children = Set.empty)
  }

  def updateChild(child: Child): Child = {
     child.isChildOf(this).fold(child)(_.update)
  }

   def isOwnedBy(thatApplicationUser: ApplicationUser): Boolean = {
      ( for {
           whoId          <- id
           thatOwnerId    <- thatApplicationUser.userId
           thisOwner      <- owner
           thisOwnerId    <- thisOwner.userId
           thisOwnerIdKey <- ownerIdKey
           redisOwnerId   <- sedis.get(thisOwnerIdKey)
             if thatOwnerId == thisOwnerId
             if thisOwnerId == redisOwnerId
           whoIdsKey      <- thatApplicationUser.whoIdsKey
        } yield sedis.sismember(whoIdsKey,whoId)
      ).contains(true)
   }
}


class WhoRepositoryComponent(whoComponents: WhoComponents) extends WhoRepository {
  implicit lazy val childRepository   = whoComponents.childRepository
  implicit lazy val partnerRepository = whoComponents.partnerRepository
  implicit lazy val searchWho         = whoComponents.searchWho
  implicit lazy val sedis             = whoComponents.sedis
  val MaxWhosToFind = whoComponents.MaxWhosToFind
}

trait WhoRepository {

  implicit val whoRepository: WhoRepository = this
  implicit def partnerRepository: PartnerRepository
  implicit def childRepository: ChildRepository
  implicit def searchWho: SearchWho
  implicit def sedis: SedisWrapper
  def MaxWhosToFind : Int

  def findAllIds(owner: ApplicationUser): Set[String] = owner.whoIdsKey.fold[Set[String]]( Set.empty )( sedis.smembers )

  def findAll(owner: ApplicationUser): Set[Who] = {
    for {
      whoId  <- findAllIds(owner).take(MaxWhosToFind)
      who    <- findById(owner,whoId)
    } yield who
  }

   def deleteAll(owner: ApplicationUser) {
      findAll(owner).take(MaxWhosToFind).foreach( _.delete(owner) )
   }

  def findLatest(owner: ApplicationUser, howMany: Int): Set[Who] = {
      findAllIds(owner).toSeq.take(howMany).reverse.flatMap( findById(owner, _) ).toSet
  }

  def findById(owner: ApplicationUser, whoId: WhoId): Option[Who] = {
    for {
      ownerId <- owner.userId
      whoEntity = new WhoEntity{ val id = Some(whoId) }
      whoIdsKey <- owner.whoIdsKey
       if sedis.sismember(whoIdsKey,whoId)
      ownerIdKey <- whoEntity.ownerIdKey
      whoOwnerId <- sedis.get(ownerIdKey)
        if whoOwnerId == ownerId
      nameKey   <- whoEntity.nameKey
      name      <- sedis.get(nameKey)
      notesKey  <- whoEntity.notesKey
      notes     =  sedis.get(notesKey)
      genderKey <- whoEntity.genderKey
      gender    =  sedis.get(genderKey).map(Gender.withName(_) )
    } yield Who( Some(owner), Some(whoId), name, Set.empty, Set.empty, notes, gender )
  }

}

class PartnerRepositoryComponent(whoComponents: WhoComponents) extends PartnerRepository {
  implicit lazy val childRepository = whoComponents.childRepository
  implicit lazy val whoRepository = whoComponents.whoRepository
  implicit lazy val sedis = whoComponents.sedis
  implicit lazy val searchWho = whoComponents.searchWho
}

trait PartnerRepository {

  implicit val partnerRepository: PartnerRepository = this
  implicit def childRepository: ChildRepository
  implicit def whoRepository: WhoRepository
  implicit def sedis: SedisWrapper
  implicit def searchWho: SearchWho

   def findPartnersOfWho(who: Who): Set[Partner] = {
      for {
         whoId: WhoId  <- who.id.toSet
         partnerIdsKey: String <- who.partnerIdsKey.toSet
         partnerId     <- sedis.smembers(partnerIdsKey)
         partner       <- findById( who, partnerId)
      } yield partner
   }

  def findById(who: Who, partnerId: String): Option[Partner] = {
    for{
      whoId <- who.id
      partnerIdsKey   <- who.partnerIdsKey
        if sedis.sismember(partnerIdsKey,partnerId)
      partnerEntity   =  new PartnerEntity{ val id = Some(partnerId) }
      partnerWhoIdKey <- partnerEntity.whoIdKey
      partnerWhoId    <- sedis.get(partnerWhoIdKey)
        if whoId == partnerWhoId
      nameKey <- partnerEntity.nameKey
      name    <- sedis.get(nameKey)
      relationshipKey <- partnerEntity.relationshipKey
      relationship = sedis.get(relationshipKey)
      notesKey     <- partnerEntity.notesKey
      notes        =  sedis.get(notesKey)
      genderKey    <- partnerEntity.genderKey
      genderName   <- sedis.get(genderKey)
      gender       =  Gender.withName(genderName)
    } yield Partner( who, Some(partnerId), name, relationship, notes, gender)
  }

}


class ChildRepositoryComponent(whoComponents: WhoComponents) extends ChildRepository {
  implicit lazy val partnerRepository = whoComponents.partnerRepository
  implicit lazy val whoRepository = whoComponents.whoRepository
  implicit lazy val sedis = whoComponents.sedis
  implicit lazy val searchWho = whoComponents.searchWho
}

trait ChildRepository {
  implicit val childRepository: ChildRepository = this
  implicit def partnerRepository: PartnerRepository
  implicit def whoRepository: WhoRepository
  implicit def sedis: SedisWrapper
  implicit def searchWho: SearchWho

   def findChildrenOfWho(who: Who): Set[Child] = {
      for {
         childrenIdsKey: String <- who.childrenIdsKey.toSet
         childId     <- sedis.smembers(childrenIdsKey)
         child       <- findById( who, childId)
      } yield child
   }

  def findById(who: Who, childId: String): Option[Child] = {
    for{
      whoId          <- who.id
      childrenIdsKey <- who.childrenIdsKey
        if sedis.sismember(childrenIdsKey,childId)
      childEntity    =  new ChildEntity{ val id = Some(childId) }
      childWhoIdKey  <- childEntity.whoIdKey
      childWhoId     <- sedis.get(childWhoIdKey)
         if whoId == childWhoId
      nameKey        <- childEntity.nameKey
      name           =  sedis.get(nameKey)
      birthYearKey   <- childEntity.birthYearKey
      birthYear      =  sedis.get(birthYearKey).map(_.toInt)
      notesKey       <- childEntity.notesKey
      notes          =  sedis.get(notesKey)
      genderKey      <- childEntity.genderKey
      genderName     <- sedis.get(genderKey)
      gender         =  Gender.withName(genderName)
    } yield Child( who, Some(childId), name, birthYear, notes, gender)
  }

}
