
import play.api._
import play.api.Play.current
import org.sedis.Pool


package object models {

  type UserId    = String
  type WhoId     = String
  type PartnerId = String
  type ChildId   = String
  type LookUpKey = String

  implicit class BooleanFold(boolean: Boolean){
    def fold[B](l: B)(r: B => B): B = if(boolean) r(l) else l
  }

  implicit class BooleanSome(boolean: Boolean){
    def some: Option[Boolean] = if(boolean) Some(true) else None
  }

}
