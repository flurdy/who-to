package models

import play.api._
import lifecycle.WhoComponents


trait SearchEntity extends RedisEntity {
   lazy val redisRootKey = id.map( userId => s"search:userId:$userId:search" )
   def searchNameKey(name: String) = redisParameterKey(s"name:$name")
}

class SearchWhoComponent(whoComponents: WhoComponents) extends SearchWho {
  val whoRepository  = whoComponents.whoRepository
  val MaxWhosToFind  = whoComponents.MaxWhosToFind
  implicit val sedis = whoComponents.sedis
}

trait SearchWho {

  def whoRepository: WhoRepository
  def MaxWhosToFind: Int
  implicit def sedis: SedisWrapper

  private def sort(whos: Seq[Who]): Seq[Who] = whos.sortBy(_.name.toLowerCase)

  private def searchByPartialNameForIds(applicationUser: ApplicationUser, searchTerm: String): Set[String] = {
    if(searchTerm.isEmpty) whoRepository.findAllIds(applicationUser)
    else {
      val nameWhoIds = searchTerm.toLowerCase.split(" +").map { subName =>
        for {
          key: String <- applicationUser.search.searchNameKey(subName).toSet
          whoId   <- sedis.smembers( key )
        } yield whoId
      }
      findCommonIdsPerName(Set.empty,nameWhoIds.toList)
    }
  }


  private def findCommonIdsPerName(commonIds: Set[String], nameWhoIds: List[Set[String]]): Set[String] = {
    nameWhoIds match {
      case Nil => commonIds
      case head::tail => {
        if(commonIds.isEmpty) findCommonIdsPerName(head,tail)
        else findCommonIdsPerName(commonIds.intersect(head),tail)
      }
    }
  }


  def searchByPartialName(applicationUser: ApplicationUser, searchTerm: String): Seq[Who] = {
    val whos = for{
      whoId  <- searchByPartialNameForIds(applicationUser, searchTerm).take(MaxWhosToFind)
      who    <- whoRepository.findById(applicationUser,whoId)
    } yield who
    sort(whos.toSeq)
  }

  def addNamesToSearchRegistry(applicationUser: ApplicationUser, whoId: String, name: String) {
    applicationUser.userId map { userId =>
      name.split(" +").foreach( subName => addNameToSearchRegistry( applicationUser.search, whoId, subName.toLowerCase ) )
    }
  }


  private def addNameToSearchRegistry(searchEntity: SearchEntity, whoId: String, name: String)(implicit sedis: SedisWrapper) {
    if(name.nonEmpty){
      searchEntity.searchNameKey(name).map { key =>
        if(!sedis.sismember(key, s"$whoId")) sedis.sadd(key, s"$whoId")
        if( name.length > 1 ){
          val nextName = name.reverse.drop(1).reverse
          addNameToSearchRegistry(searchEntity, whoId, nextName)
        }
      }
    }
  }

  def updateNamesInSearchRegistry(applicationUser: ApplicationUser, whoId: String,
                                  oldName: String, newName: String)(implicit sedis: SedisWrapper) {
    if( oldName != newName ){
      removeNamesFromSearchRegistry(applicationUser,whoId,oldName)
      addNamesToSearchRegistry(applicationUser,whoId,newName)
    }
  }


  def removeNamesFromSearchRegistry(applicationUser: ApplicationUser, whoId: String, name: String)(implicit sedis: SedisWrapper) {
    applicationUser.userId map { userId =>
      for{
        subName <- name.split(" +")
      } yield removeNameFromSearchRegistry(userId, whoId, subName.toLowerCase)
    }
  }


  private def removeNameFromSearchRegistry(userId: String, whoId: String, name: String)(implicit sedis: SedisWrapper){
    if(!name.isEmpty){
      if(sedis.sismember(s"search:appUser:$userId:search:name:$name", s"$whoId")){
        sedis.srem(s"search:appUser:$userId:search:name:$name", s"$whoId")
      } else Logger.error(s"whoid $whoId no longer in search registry for $name")
      removeNameFromSearchRegistry(userId, whoId, name.reverse.drop(1).reverse)
    }
  }

}

