package models

import play.api._
import play.api.Play.current
import org.sedis.Pool


trait RedisEntity {
   def id: Option[String]
   def redisRootKey: Option[LookUpKey]
   def redisParameterKey(parameter: String) = {
      val key = redisRootKey.map( key => s"$key:$parameter" )
      // println(s"Redis: for $parameter the key is $key")
      key
   }
}


trait SedisAccess {

   def pool: Pool

   def get(key: String): Option[String]   = pool.withClient( _.get(key) )

   def smembers(key: String): Set[String] = pool.withClient( _.smembers(key) )

   def sismember(key: String, id: String): Boolean = pool.withClient( _.sismember(key,id) )

   def set(key: String, value: String) = {
      // println(s"Redis: setting $key as $value")
      pool.withClient( _.set(key,value) )
   }

   def sadd(key: String, value: String) = {
      // println(s"Redis: Adding $key as $value")
      pool.withClient( _.sadd(key,value) )
   }

   def del(key: String) = pool.withClient( _.del(key) )

   def incr(key: String) = pool.withClient( _.incr(key) )

   def srem(key: String, id: String) = pool.withClient( _.srem(key,id  ) )

}


class SedisWrapper (val pool: Pool) extends SedisAccess
