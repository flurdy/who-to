package controllers

import play.api._
import play.api.Play._
import play.api.mvc._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.Results._
import play.api.data._
import play.api.data.Forms._
import models._
import jp.t2v.lab.play2.auth._


trait SearchForm {

  val searchForm = Form(
    "searchtext" -> optional(text(maxLength = 100))
  )

  implicit val searchWho: SearchWho
}


class SearchController ()
  (implicit val messagesApi: MessagesApi, val userCredentialsRepository: UserCredentialsRepository,
   val whoRepository: WhoRepository, val sedis: SedisWrapper, val searchWho: SearchWho)
  extends Controller with SearchForm with AuthenticatedUser with I18nSupport with WithSedis with AuthElement with CustomAuthConfig {

  def showSearch = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    val whos = for{
       user <- currentApplicationUser.toSeq
       who  <- whoRepository.findLatest(user, 15)
    } yield who
    Ok(views.html.search.searchfront(searchForm,whos))
  }

  def findWho = StackAction(AuthorityKey -> NormalUser) { implicit request =>
    searchForm.bindFromRequest.fold (
      formWithErrors => BadRequest(views.html.search.searchfront(formWithErrors)),
      searchTerm => {
         val whos = if(searchTerm.fold(true)(_.isEmpty)){
            for{
               user <- currentApplicationUser.toSeq
               who  <- whoRepository.findAll(user)
            } yield who
         } else {
            for {
               user <- currentApplicationUser.toSeq
               term <- searchTerm.toSeq
               who  <- searchWho.searchByPartialName( user, term)
            } yield who
         }
         val sortedWhos = whos.sortBy(_.name.toLowerCase)
         Ok(views.html.search.searchfront(searchForm.fill(searchTerm), sortedWhos))
      }
    )
  }
}
