package controllers

import play.api._
import play.api.Play._
import play.api.mvc._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.Results._
import play.api.data._
import play.api.data.Forms._
import models._
import jp.t2v.lab.play2.auth._
import jp.t2v.lab.play2.stackc._


trait WhoLookUp extends Controller with I18nSupport {

  implicit def whoRepository: WhoRepository
  implicit def sedis: SedisWrapper
  implicit def currentApplicationUser(implicit req: RequestWithAttributes[_]): Option[ApplicationUser]

  def WithWho(whoId: String)(block: Who => Result)(implicit req: RequestWithAttributes[_]) = {
    currentApplicationUser.fold[Result](Unauthorized(views.html.error.unauthorized())){ currentUser =>
      whoRepository.findById(currentUser,whoId).fold[Result](NotFound(views.html.error.notfound())){ who: Who =>
        block(who)
      }
    }
  }

}

class WhoController ()(implicit val messagesApi: MessagesApi, val sedis: SedisWrapper,
  val userCredentialsRepository: UserCredentialsRepository, val whoRepository: WhoRepository,
  partnerRepository: PartnerRepository, childRepository: ChildRepository, val searchWho: SearchWho)
extends Controller with AuthenticatedUser with SearchForm with I18nSupport with WithSedis
   with AuthElement with CustomAuthConfig with WhoLookUp {

  val addWhoForm = Form( mapping(
    "name" -> nonEmptyText(maxLength = 100)
  ) (name => new Who(None,name))(who => Some(who.name)))

  val nameForm = Form(
    "name" -> nonEmptyText(maxLength = 100)
  )

  val notesForm = Form(
    "notes" -> nonEmptyText(maxLength = 1999)
  )


  def addWho = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    addWhoForm.bindFromRequest.fold (
      formWithErrors => BadRequest(views.html.search.searchfront(searchForm)),
      who => {
          val whoId = for {
            currentUser <- currentApplicationUser
            updatedWho  <- who.save(currentUser)
            whoId       <- updatedWho.id
          } yield whoId
          whoId.fold[Result]( InternalServerError(views.html.error.internalserver()) ) { id =>
             Redirect(routes.WhoController.showWho(id)).flashing("success-message" -> "Who added")
          }
      }
    )
  }


  def showWho(whoId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWho(whoId) { implicit who: Who =>
      val fatWho = who.populateWithPartners.populateWithChildren
      Ok(views.html.who.showWho(fatWho,nameForm,notesForm))
    }
  }


  def updateName(whoId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWho(whoId) { implicit who: Who =>
      nameForm.bindFromRequest.fold (
        formWithErrors =>    {
          val fatWho = who.populateWithPartners
          BadRequest(views.html.who.showWho(fatWho,formWithErrors,notesForm))
        },
        name => {
          who.updateName(currentApplicationUser.get,name)
          Redirect(routes.WhoController.showWho(whoId))
        }
     )
   }
  }


  def updateNotes(whoId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWho(whoId) { implicit who: Who =>
       notesForm.bindFromRequest.fold (
          formWithErrors =>    {
            val fatWho = who.populateWithPartners
            BadRequest(views.html.who.showWho(fatWho,nameForm,formWithErrors))
          },
          notes => {
            who.updateNotes(currentApplicationUser.get,notes)
            Redirect(routes.WhoController.showWho(whoId))
          }
       )
    }
  }


  def deleteWho(whoId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWho(whoId) { implicit who: Who =>
      who.delete(currentApplicationUser.get)
      Redirect(routes.Application.index)
    }
  }

}
