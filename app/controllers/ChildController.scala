package controllers

import play.api._
import play.api.Play._
import play.api.mvc._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.Results._
import play.api.data._
import play.api.data.Forms._
import models._
import jp.t2v.lab.play2.auth._
import jp.t2v.lab.play2.stackc.{RequestWithAttributes, RequestAttributeKey, StackableController}



trait ChildLookUp extends WhoLookUp {

  def WithWhoAndChild(whoId: String, childId: String)(block: Who => Child => Result)(implicit req: RequestWithAttributes[_]) = {
    WithWho(whoId){ who =>
      who.findChildById(childId).fold[Result](NotFound(views.html.error.notfound())){ child =>
        block(who)(child)
      }
    }
  }
}

class ChildController ()
(implicit val messagesApi: MessagesApi, val sedis: SedisWrapper, val userCredentialsRepository: UserCredentialsRepository,
  val whoRepository: WhoRepository, partnerRepository: PartnerRepository, childRepository: ChildRepository, searchWho: SearchWho)
extends Controller with AuthenticatedUser with I18nSupport with WithSedis with AuthElement with CustomAuthConfig with ChildLookUp {

  val childForm = Form( mapping(
    "who" -> mapping(
      "id" -> nonEmptyText(maxLength = 100),
      "name" -> nonEmptyText(maxLength = 100)
    )( (id,name)=> new Who(Some(id),name) )( who => Some((who.id.get,who.name))),
    "id" -> optional(text(maxLength = 100)),
    "name" -> optional(text(minLength = 2, maxLength = 100)),
    "birthyear" -> optional(number),
    "notes" -> optional(text),
    "gender" -> mapping(
      "name" -> nonEmptyText
    )(Gender.withName)( gender => Some(gender.toString))
  )( Child.apply )( Child.unapply ))


  def showAddChildToWho(whoId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWho(whoId) { implicit who: Who =>
      Ok(views.html.who.child.showAddChild( who, childForm))
    }
  }

  def addChildToWho(whoId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWho(whoId) { implicit who: Who =>
      childForm.bindFromRequest.fold (
        formWithErrors => BadRequest(views.html.who.child.showAddChild(who,formWithErrors)),
        formData => {
          who.addChild(formData)
          Redirect(routes.WhoController.showWho(whoId))
        }
      )
    }
  }


  def showEditChildOfWho(whoId: String, childId: String) =  StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWhoAndChild(whoId,childId) { implicit who: Who => child =>
      Ok(views.html.who.child.showEditChild( who, child, childForm.fill(child)))
    }
  }


  def removeChildFromWho(whoId: String, childId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWhoAndChild(whoId,childId) { implicit who: Who => child =>
      who.removeChild(child)
      Redirect(routes.WhoController.showWho(whoId))
    }
  }


  def updateChildOfWho(whoId: String, childId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWhoAndChild(whoId,childId) { implicit who: Who => child =>
      childForm.bindFromRequest.fold (
        formWithErrors => BadRequest(views.html.who.child.showEditChild( who, child, formWithErrors)),
        formData => {
          for{
            formChildId <- formData.id
            formWhoId <- formData.who.id
            if childId == formChildId
            if whoId == formWhoId
          } yield who.updateChild(formData)
          Redirect(routes.WhoController.showWho(whoId))
        }
      )
    }
  }

}
