package controllers

import play.api._
import play.api.Play._
import play.api.mvc._
import play.api.mvc.Results._
import play.api.data._
import play.api.data.Forms._
import models._
import jp.t2v.lab.play2.auth._
import jp.t2v.lab.play2.stackc._
import reflect.classTag
import reflect.ClassTag
import scala.concurrent.{ExecutionContext, Future}


trait AuthenticatedUser {
  implicit def loggedIn(implicit req: RequestWithAttributes[_]): UserCredentials
  implicit def currentApplicationUser(implicit req: RequestWithAttributes[_]): Option[ApplicationUser] = Some(loggedIn.user)
}


trait OptionalAuthenticatedUser {
  implicit def loggedIn[A](implicit req: RequestWithAttributes[A]): Option[UserCredentials]
  implicit def currentApplicationUser[A](implicit req: RequestWithAttributes[A]): Option[ApplicationUser] = loggedIn.map(_.user)
}


trait NotAuthenticatedUser {
  implicit def currentApplicationUser: Option[ApplicationUser] = None
}


trait CustomAuthConfig extends AuthConfig with WithSedis {

  def userCredentialsRepository: UserCredentialsRepository

  type Id = String

  type User = UserCredentials

  type Authority = Permission


  val idTag: ClassTag[Id] = classTag[Id]

  val sessionTimeoutInSeconds : Int = 3600


  def resolveUser(id: Id)(implicit context: ExecutionContext): Future[Option[User]] =
    Future.successful( userCredentialsRepository.findById(id) )

  def loginSucceeded(request: RequestHeader)(implicit context: ExecutionContext): Future[Result] = {
    // Logger.info("Login succeeded")
    val uri = request.session.get("access_uri").getOrElse(routes.Application.index.url.toString)
     Future.successful(
        Redirect(uri).withSession(request.session - "access_uri").flashing("success-message"->"login.success")
     )
  }

  def logoutSucceeded(request: RequestHeader)(implicit context: ExecutionContext): Future[Result] = {
    Future.successful ( Redirect(routes.Application.index) )
  }

  def authenticationFailed(request: RequestHeader)(implicit context: ExecutionContext): Future[Result] = {
    Logger.warn("Authentication failed")
    Future.successful {
      Redirect(routes.RegistrationAuthentication.showLogin).withSession("access_uri" -> request.uri)
    }
  }

  def authorizationFailed(request: RequestHeader, user: User, authority: Option[Authority])(implicit context: ExecutionContext): Future[Result] = {
    Logger.warn("Authorization failed")
    Future.successful {
      Redirect(routes.RegistrationAuthentication.showLogin).withSession("access_uri" -> request.uri)
    }
  }

  def authorize(user: User, authority: Authority)(implicit context: ExecutionContext): Future[Boolean] = {
    Future.successful {
      // Logger.info("Authorize lookup")
      (user.permission, authority) match {
        case (Some(Administrator), _)       => true
        case (Some(NormalUser), NormalUser) => true
        // case (_, Anyone)                    => true
        case _                              => false
      }
    }
  }

  // override lazy val cookieSecureOption: Boolean =
  //    play.api.Play.current.configuration.getBoolean("auth.cookie.secure").getOrElse(false)

  // override lazy val idContainer: IdContainer[Id] = new CookieIdContainer[Id]
  override lazy val idContainer: AsyncIdContainer[Id] = AsyncIdContainer(new CookieIdContainer[Id])

}
