package controllers

import play.api._
import play.api.Play._
import play.api.mvc._
import play.api.mvc.Results._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.data._
import play.api.data.Forms._
import models._
import jp.t2v.lab.play2.auth._
import jp.t2v.lab.play2.stackc._
import scala.concurrent.{ExecutionContext, Future}


trait ProfileLookUp extends Controller with I18nSupport {

  implicit def applicationUserRepository: ApplicationUserRepository
  implicit def sedis: SedisWrapper
  implicit def currentApplicationUser(implicit req: RequestWithAttributes[_]): Option[ApplicationUser]

  def WithProfile(profileId: String)(block: ApplicationUser => Result)(implicit req: RequestWithAttributes[_]) = {
    currentApplicationUser.fold[Result](Unauthorized(views.html.error.unauthorized())){ currentUser =>
      currentUser.userId.filter( _ == profileId ).fold[Result](Unauthorized(views.html.error.unauthorized())){ _ =>
        applicationUserRepository.findById(profileId).fold[Result](NotFound(views.html.error.notfound())){ profile: ApplicationUser =>
          block(profile)
        }
      }
    }
  }

  def WithAsyncProfile(profileId: String)(block: ApplicationUser => Future[Result])(implicit req: RequestWithAttributes[_]) = {
    currentApplicationUser.fold[Future[Result]](Future.successful(Unauthorized(views.html.error.unauthorized()))){ currentUser =>
      currentUser.userId.filter( _ == profileId ).fold[Future[Result]](Future.successful(Unauthorized(views.html.error.unauthorized()))){ _ =>
        applicationUserRepository.findById(profileId).fold[Future[Result]](Future.successful(NotFound(views.html.error.notfound()))){ profile: ApplicationUser =>
          block(profile)
        }
      }
    }
  }
}

class ProfileController ()
    (implicit val messagesApi: MessagesApi, val sedis: SedisWrapper, val applicationUserRepository: ApplicationUserRepository,
        val userCredentialsRepository: UserCredentialsRepository, val whoRepository: WhoRepository)
extends Controller with AuthenticatedUser with I18nSupport with AuthElement with CustomAuthConfig with ProfileLookUp with LoginLogout {

  val profileUserMapping = mapping(
      "email" -> optional(email),
      "fullname" -> optional(text)
    )( (email,fullname) => new ApplicationUser("",email,fullname)
    )( applicationUser => Some((applicationUser.email,applicationUser.fullname)) )

  val profileForm = Form( profileUserMapping )

  def showProfile(profileId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithProfile(profileId){ profile =>
      val prefilledForm = profileForm.fill(profile)
      Ok(views.html.profile.profile(profile,profileForm))
    }
  }


  def updateProfile(profileId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithProfile(profileId){ profile =>
      profileForm.bindFromRequest.fold (
        formWithErrors => BadRequest(views.html.profile.profile(profile,formWithErrors)),
        maybeValue => {
          applicationUserRepository.findByUsername(profile.username) map { appUser =>
            appUser.copy(fullname = maybeValue.fullname)
                   .updateFullname
                   .copy(email = maybeValue.email)
                   .updateEmail
          }
          Redirect(routes.ProfileController.showProfile(profileId)).flashing("success-message" -> "Profile updated")
        }
      )
    }
  }


  val changePasswordMapping = tuple (
    "username" -> nonEmptyText,
    "oldPassword" -> nonEmptyText,
    "newPassword" -> nonEmptyText,
    "confirmPassword" -> nonEmptyText
  ) verifying( "Passwords did not match. Please enter passwords again", fields => fields match {
      case (username,oldPassword,newPassword,confirmPassword) => newPassword == confirmPassword
  }) verifying("Authentication failed. Please verify your old password", fields => fields match {
    case (username,oldPassword,newPassword,confirmPassword) =>  userCredentialsRepository.authenticate(username,oldPassword).isDefined
  })

  val passwordForm = Form( changePasswordMapping )

  def showChangePassword(profileId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    Ok(views.html.profile.password(currentApplicationUser.get,passwordForm))
  }

  def changePassword(profileId: String) = AsyncStack(AuthorityKey -> NormalUser){ implicit request =>
    WithAsyncProfile(profileId){ profile =>
      import scala.concurrent.ExecutionContext.Implicits.global
      passwordForm.bindFromRequest.fold (
        formWithErrors => Future.successful(BadRequest(views.html.profile.password(profile,formWithErrors))),
        maybeValue => {
          userCredentialsRepository.findById(profileId) map { credentials =>
            credentials.copy(password = Some(maybeValue._3)).changePassword
          }
          Future.successful(Redirect(routes.RegistrationAuthentication.logout).flashing("success-message" -> "Password changed. Please log in again"))
        }
      )
    }
  }

  def showDeleteProfile(profileId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithProfile(profileId){ profile =>
      Ok(views.html.profile.delete(profile))
    }
  }

  def deleteProfile(profileId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithProfile(profileId){ profile =>
      userCredentialsRepository.findById(profileId) map ( _.delete )
      Redirect(routes.RegistrationAuthentication.logout)
    }
  }

}



class OpenProfileController ()(implicit val messagesApi: MessagesApi, val sedis: SedisWrapper,
  val applicationUserRepository: ApplicationUserRepository,
  val userCredentialsRepository: UserCredentialsRepository)
  extends Controller with I18nSupport with OptionalAuthenticatedUser with OptionalAuthElement with CustomAuthConfig {

  def showResetPassword = StackAction { implicit request =>
    Ok(views.html.profile.reset(resetPasswordForm))
  }

  def showPasswordReset = StackAction { implicit request =>
    Ok(views.html.profile.passwordReset())
  }

  val resetPasswordForm = Form( tuple(
      "username" -> nonEmptyText,
      "email" -> email
  ) )

  def resetPassword = StackAction { implicit request =>
    currentApplicationUser match {
      case Some(currentUser) => Redirect(routes.Application.index)
      case None => {
          // TODO
          Logger.warn("Resetting password postponed until email is integrated")
          Redirect(routes.OpenProfileController.showPasswordReset()).flashing("info-message" -> "Password not changed")
      }
    }
  }

}


