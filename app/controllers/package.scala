import models._

package controllers {

   trait WithSedis {

     implicit def sedis: SedisWrapper

   }

}
