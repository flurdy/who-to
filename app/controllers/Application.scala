package controllers

import play.api._
import play.api.Play._
import play.api.mvc._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.Results._
import play.api.data._
import play.api.data.Forms._
import models._
import jp.t2v.lab.play2.auth._


class Application ()(implicit val messagesApi: MessagesApi, val sedis: SedisWrapper, val whoRepository: WhoRepository,
    val applicationUserRepository: ApplicationUserRepository, val userCredentialsRepository: UserCredentialsRepository)
  extends Controller with OptionalAuthenticatedUser with I18nSupport with RegistrationForm with LoginForm
   with WithSedis with OptionalAuthElement with CustomAuthConfig {

  def index = StackAction { implicit request =>
    currentApplicationUser match {
      case Some(_) => Redirect(routes.SearchController.showSearch)
      case _       => Ok(views.html.index( simpleRegistrationForm, loginForm ) )
    }
  }

   def about = StackAction { implicit request =>
      Ok(views.html.about_contact())
   }

}
