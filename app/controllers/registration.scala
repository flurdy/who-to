package controllers

import play.api._
import play.api.Play._
import play.api.mvc._
import play.api.mvc.Results._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.data._
import play.api.data.Forms._
import models._
import jp.t2v.lab.play2.auth._
import scala.concurrent.{future, ExecutionContext, Future}


trait RegistrationForm {

  implicit def sedis: SedisWrapper
  implicit def applicationUserRepository: ApplicationUserRepository
  implicit def userCredentialsRepository: UserCredentialsRepository
  implicit def whoRepository: WhoRepository

  val applicationUserMapping = mapping(
    "username" -> text.verifying(
      "Username is not available. Please choose another",
      applicationUserRepository.isUsernameValidAndAvailable(_)
    ),
    "email" -> optional(email),
    "fullname" -> optional(text)
  )( (username,email,fullname) => new ApplicationUser(username,email,fullname)
  )( applicationUser => Some((applicationUser.username,applicationUser.email,applicationUser.fullname)) )


  val passwordMapping = tuple (
    "password" -> nonEmptyText,
    "confirmPassword" -> nonEmptyText
    ) verifying( "Passwords did not match. Please enter passwords again", fields => fields match {
      case (password,confirmPassword) => password == confirmPassword
  })

  val userCredentialsMapping = mapping(
    "applicationUser" -> applicationUserMapping,
    "passwords" -> passwordMapping
  )( (user,passwords) => UserCredentials(user, Some(passwords._1),None)
  )( userCredentials => Some(userCredentials.user,("","")) )

  val simpleRegistrationForm = Form( applicationUserMapping )

  val fullRegistrationForm = Form( userCredentialsMapping )

}

trait LoginForm {

  implicit def sedis: SedisWrapper
  implicit def userCredentialsRepository: UserCredentialsRepository

  val loginMapping = tuple (
    "username" -> nonEmptyText ,
    "password" -> nonEmptyText
  ) verifying("Authentication failed. Please verify your credentials or register", fields => fields match {
    case (username,password) => userCredentialsRepository.authenticate(username,password).isDefined
  })

  val loginForm = Form( loginMapping )
}



class RegistrationAuthentication ()
  (implicit val messagesApi: MessagesApi, val sedis: SedisWrapper, val whoRepository: WhoRepository,
    val applicationUserRepository: ApplicationUserRepository, val userCredentialsRepository: UserCredentialsRepository)
  extends Controller with I18nSupport with NotAuthenticatedUser with RegistrationForm with LoginForm with LoginLogout with CustomAuthConfig {
 import ExecutionContext.Implicits.global

  def showRegistration = Action { implicit request =>
    Ok(views.html.register(simpleRegistrationForm,fullRegistrationForm))
  }


  def redirectToRegistration = Action { implicit request =>
    Redirect(routes.RegistrationAuthentication.showRegistration)
  }


  def startRegistrationProcess() = Action { implicit request =>
    simpleRegistrationForm.bindFromRequest.fold (
      formWithErrors => BadRequest(views.html.register(formWithErrors,fullRegistrationForm)),
      maybeValue => {
        val prefilled = fullRegistrationForm.fill(  new UserCredentials(maybeValue) )
        Ok(views.html.register(simpleRegistrationForm,prefilled))
      }
    )
  }


  def finishRegistrationProcess() = Action.async { implicit request =>
    fullRegistrationForm.bindFromRequest.fold (
      formWithErrors => Future.successful( BadRequest(views.html.register(simpleRegistrationForm,formWithErrors)) ),
      registrationCredentials => {
        registrationCredentials.register.user.userId.fold[Future[Result]]
                  { Future.successful(InternalServerError) }
                  { gotoLoginSucceeded }
      }
    )
  }


  def showLogin = Action { implicit request =>
    Ok(views.html.login(loginForm))
  }


  def loginProcess() = Action.async { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => Future.successful( BadRequest(views.html.login(formWithErrors)) ),
      loginValues => {
        applicationUserRepository.findByUsername(loginValues._1) flatMap { _.userId } match {
          case Some(userId) => gotoLoginSucceeded(userId)
          case _ => Future.successful( BadRequest(views.html.index( simpleRegistrationForm,loginForm.fill((loginValues._1,"")))))
        }
      }
    )
  }


  def logout = Action.async { implicit request =>
    val flashMessage = request.flash.get("success-message").getOrElse(
                       request.flash.get("info-message").getOrElse("Logged out"))
    gotoLogoutSucceeded.map(_.flashing("info-message" -> flashMessage))
  }

}
