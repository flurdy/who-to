package controllers

import play.api._
import play.api.Play._
import play.api.mvc._
import play.api.i18n.{I18nSupport, MessagesApi
}
import play.api.mvc.Results._
import play.api.data._
import play.api.data.Forms._
import models._
import javax.inject._
import jp.t2v.lab.play2.auth._
import jp.t2v.lab.play2.stackc._


trait PartnerLookUp extends WhoLookUp {

  def WithWhoAndPartner(whoId: String, partnerId: String)(block: Who => Partner => Result)(implicit req: RequestWithAttributes[_]) = {
    WithWho(whoId){ who: Who =>
      who.findPartnerById(partnerId).fold[Result](NotFound(views.html.error.notfound())){ partner =>
        block(who)(partner)
      }
    }
  }
}

class PartnerController ()
(implicit val messagesApi: MessagesApi, val sedis: SedisWrapper, val userCredentialsRepository: UserCredentialsRepository,
  val whoRepository: WhoRepository, partnerRepository: PartnerRepository, childRepository: ChildRepository, searchWho: SearchWho)
  extends Controller with AuthenticatedUser with I18nSupport with WithSedis with AuthElement with CustomAuthConfig with PartnerLookUp {

  val partnerForm = Form( mapping(
    "who" -> mapping(
      "id"   -> nonEmptyText(maxLength = 100),
      "name" -> nonEmptyText(maxLength = 100)
    )( (id,name)=> new Who(Some(id), name) )( who => Some((who.id.get,who.name))),
    "id"     -> optional(text(maxLength = 100)),
    "name"   -> nonEmptyText(minLength = 2, maxLength = 100),
    "relationship" -> optional(text),
    "notes"  -> optional(text),
    "gender" -> mapping(
      "name" -> nonEmptyText
    )(Gender.withName)( gender => Some(gender.toString))
  )( Partner.apply )( Partner.unapply ))


  def showAddPartnerToWho(whoId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWho(whoId) { implicit who: Who =>
      Ok(views.html.who.partner.showAddPartner(who,partnerForm))
    }
  }


  def addPartnerToWho(whoId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWho(whoId) { implicit who: Who =>
        partnerForm.bindFromRequest.fold (
          formWithErrors => BadRequest(views.html.who.partner.showAddPartner(who,formWithErrors)),
          partnerFormData => {
            who.addPartner(partnerFormData)
            Redirect(routes.WhoController.showWho(whoId))
          }
        )
    }
  }


  def showEditPartnerOfWho(whoId: String, partnerId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWhoAndPartner(whoId,partnerId) { implicit who: Who => partner =>
      Ok(views.html.who.partner.showEditPartner( who, partner, partnerForm.fill(partner)))
    }
  }


  def updatePartnerOfWho(whoId: String, partnerId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWhoAndPartner(whoId,partnerId) { implicit who: Who => partner =>
      partnerForm.bindFromRequest.fold (
        formWithErrors => BadRequest(views.html.who.partner.showEditPartner( who, partner, formWithErrors)),
        partnerFormData => {
          for{
            formPartnerId <- partnerFormData.id
            formWhoId <- partnerFormData.who.id
            if partnerId == formPartnerId
            if whoId == formWhoId
          } yield who.updatePartner(partnerFormData)
          Redirect(routes.PartnerController.showEditPartnerOfWho(whoId,partnerId))
        }
      )
    }
  }


  def removePartnerFromWho(whoId: String, partnerId: String) = StackAction(AuthorityKey -> NormalUser){ implicit request =>
    WithWhoAndPartner(whoId,partnerId) { implicit who: Who => partner =>
      who.removePartner(partner)
      Redirect(routes.WhoController.showWho(whoId))
    }
  }

}
