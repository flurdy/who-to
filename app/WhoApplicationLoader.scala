package lifecycle

import play.api._
import play.api.ApplicationLoader.Context
import router.Routes
import models._
import org.sedis.Pool
import com.typesafe.play.redis.RedisCacheComponents
import play.api.i18n._
import controllers._

class WhoApplicationLoader extends ApplicationLoader {
  def load(context: Context) = {
    new WhoComponents(context).application
  }
}

class WhoComponents(context: Context) extends BuiltInComponentsFromContext(context) with I18nComponents with RedisCacheComponents {
   implicit val whoComponents: WhoComponents = this
   val MaxWhosToFind = 50
   implicit lazy val whoRepository     = new WhoRepositoryComponent(this)
   implicit lazy val partnerRepository = new PartnerRepositoryComponent(this)
   implicit lazy val childRepository   = new ChildRepositoryComponent(this)
   implicit lazy val searchWho         = new SearchWhoComponent(this)
   implicit lazy val applicationUserRepository = new ApplicationUserRepositoryComponent(this)
   implicit lazy val userCredentialsRepository = new UserCredentialsRepositoryComponent(this)
   implicit lazy val sedis             = new SedisWrapper(sedisPool)
   implicit def messages: MessagesApi = messagesApi
   lazy val applicationController      = new controllers.Application()
   lazy val searchController           = new SearchController()
   lazy val registrationAuthentication = new RegistrationAuthentication()
   lazy val profileController          = new ProfileController()
   lazy val openProfileController      = new OpenProfileController()
   lazy val whoController              = new WhoController()
   lazy val partnerController          = new PartnerController()
   lazy val childController            = new ChildController()
   lazy override val httpErrorHandler  = new ErrorHandler()
   lazy val assets                     = new Assets(httpErrorHandler)
   lazy val router                     = new Routes(httpErrorHandler, applicationController, searchController,
                                            registrationAuthentication, profileController, openProfileController, whoController,
                                            partnerController, childController, assets)
}
