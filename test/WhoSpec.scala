package models

import org.specs2.specification.Scope
import org.specs2.mock._
import org.specs2.mutable._
import org.mockito.Matchers.{eq => eqTo}
import org.sedis.Pool
import redis.clients.jedis.{Jedis,JedisPool}
import scala.collection.JavaConversions._

class WhoSpec extends Specification with Mockito {


   trait Setup extends Scope {
      import Gender._
      implicit val whoRepositoryMock = mock[WhoRepository]
      implicit val partnerRepositoryMock = mock[PartnerRepository]
      implicit val applicationUserRepositoryMock = mock[ApplicationUserRepository]
      implicit val childRepositoryMock = mock[ChildRepository]
      implicit val searchWhoMock = mock[SearchWho]
      implicit val sedisWrapperMock = mock[SedisWrapper]

      sedisWrapperMock.get("who:whoId:1:owner:ownerId") returns Some("someUserId")
      sedisWrapperMock.get("who:whoId:2:owner:ownerId") returns Some("someUserId")
      sedisWrapperMock.sismember("applicationUser:userId:someUserId:who.whoIds","1") returns true
      sedisWrapperMock.sismember("applicationUser:userId:someUserId:who.whoIds","2") returns true


      val appUser =  ApplicationUser( Some("someUserId"), "username", Some("email"), Some("fullname"))
      val who1 = new Who(Some("1"),"who 1").copy(owner = Some(appUser))
      val who2 = new Who(Some("2"),"who 2").copy(owner = Some(appUser))
      val who3 = new Who(Some("3"),"who 3").copy(owner = Some(appUser))
      val partner1: Partner = Partner(who1, None, "partner 1", None, None, Male)
      val partner2: Partner = Partner(who1, None, "partner 2", None, None, Female)
      val child1: Child = Child(who1, None, Some("Child 1"), None, None, Male)
      val child2: Child = Child(who1, None, Some("Child 2"), None, None, Female)
   }

   "Who" should {

      "have no partners initially" in new Setup {
         who1.partners must be empty
      }

      "have no children initially" in new Setup {
         who1.children must be empty
      }

      "can change name" in new Setup {
         val newWho1 = new Who(Some("1"),"who 1+").copy(owner = Some(appUser))
         sedisWrapperMock.get("who:whoId:1:name") returns Some("who 1")

         who1.updateName(appUser,"who 1+")

         there was one(sedisWrapperMock).set("who:whoId:1:name","who 1+")
      }

      "can be deleted" in new Setup {

         who1.delete(appUser)

         there was one(sedisWrapperMock).del("who:whoId:1:name")
      }

      "when adding two partners show two partners" in new Setup {

         sedisWrapperMock.incr("partner:maxPartnerId") returns 99 thenReturns 105

         who1.addPartner(partner1)
            .addPartner(partner2)
            .partners must haveSize(2)
      }

      "when removing one of partners show one partner" in new Setup {

         sedisWrapperMock.incr("partner:maxPartnerId") returns 99 thenReturns 100
         sedisWrapperMock.get("partner:partnerId:99:who:whoId") returns Some("1")
         sedisWrapperMock.sismember("who:whoId:1:partner:partnerIds","99") returns true

         who1.addPartner(partner1)
            .addPartner(partner2)
            .removePartner(partner1.copy(id = Some("99")))
            .partners must haveSize(1)

         there was one(sedisWrapperMock).del("partner:partnerId:99:name")
         there was one(sedisWrapperMock).srem("who:whoId:1:partner:partnerIds","99")
      }

      "when adding two children show two children" in new Setup {
         sedisWrapperMock.incr("child:maxChildId") returns 33 thenReturns 44

         who1.addChild(child1)
            .addChild(child2)
            .children must haveSize(2)
      }

      "when removing one of two children show one child" in new Setup {
         sedisWrapperMock.incr("child:maxChildId") returns 55 thenReturns 88
         sedisWrapperMock.get("child:childId:88:who:whoId") returns Some("1")
         sedisWrapperMock.sismember("who:whoId:1:child:childrenIds","88") returns true

         who1.addChild(child1)
            .addChild(child2)
            .removeChild(child2.copy(id = Some("88")))
            .children must haveSize(1)

         there was one(sedisWrapperMock).del("child:childId:88:name")
         there was one(sedisWrapperMock).srem("who:whoId:1:child:childrenIds","88")
      }
   }

   "partner" should {

      "update name" in new Setup {
         sedisWrapperMock.incr("partner:maxPartnerId") returns 99

         who1.addPartner(partner1)
         partner1.copy(id = Some("99"), name = "Another").update

         there was one(sedisWrapperMock).set("partner:partnerId:99:name","Another")
      }
   }

   "child" should {

      "update name" in new Setup {
         sedisWrapperMock.incr("child:maxChildId") returns 99

         who1.addChild(child1)
         child1.copy(id = Some("99"), name = Some("Another")).update

         there was one(sedisWrapperMock).set("child:childId:99:name","Another")
      }
   }
}
