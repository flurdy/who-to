package models

import org.specs2.specification.{Scope,Tables}
import org.specs2.mock._
import org.specs2.mutable._
import org.mockito.Matchers.{eq => eqTo}
import org.sedis.Pool
import redis.clients.jedis.{Jedis,JedisPool}
import scala.collection.JavaConversions._

class ApplicationUserSpec extends Specification with Mockito with Tables {

   trait Setup extends Scope {
      implicit val whoRepositoryMock = mock[WhoRepository]
      implicit val sedisMock = mock[SedisWrapper]
      implicit val applicationUserRepository  = new ApplicationUserRepository {
         implicit val whoRepository = whoRepositoryMock
         implicit val sedis = sedisMock
      }
   }


   "isUsernameValid" should {

      "accept usernames" in new Setup {
         "username"              | "result" |
         "Jonno"                 ! true     |
         " ggghhhh"              ! true     |
         "john@example.com"      ! true     |
         "john+mark@example.com" ! true     |
         "john_exa----com"       ! true     |> {
            (username: String, result: Boolean) =>
               applicationUserRepository.isUsernameValid(username) must beEqualTo( result )
         }
      }

      "not accept usernames" in new Setup {
         "username"              | "result"  |
         ""                      ! false     |
         "aa"                    ! false     |
         "john jon"              ! false     |
         "john()jon"             ! false     |
         "johnåjon"              ! false     |> {
            (username: String, result: Boolean) =>
               applicationUserRepository.isUsernameValid(username) must beEqualTo( result )
         }
      }

      "not accept null username" in new Setup {
         applicationUserRepository.isUsernameValid(null) must beFalse
      }
   }

   "isUsernameBlacklisted" should {

      "blacklist usernames" in new Setup {
         "username" | "result" |
         "support"  ! true     |
         "test"     ! true     |
         "admin"    ! true     |> { (username: String,result: Boolean) =>
            applicationUserRepository.isUsernameBlacklisted(username) must beEqualTo( result )
         }
      }

      "blacklist stem usernames" in new Setup {
         "username"         | "result" |
         "supporters"       ! true     |
         "test123"          ! true     |
         "administrator"    ! true     |> { (username: String,result: Boolean) =>
            applicationUserRepository.isUsernameBlacklisted(username) must beEqualTo( result )
         }
      }

      "not blacklist normal username" in new Setup {
         applicationUserRepository.isUsernameBlacklisted("jonoo") must beFalse
      }
   }

   "isUsernameAlreadyRegistered" should {

      "allow non registered usernames" in new Setup {
         sedisMock.get(anyString) returns None

         applicationUserRepository.isUsernameAlreadyRegistered("popular") must beFalse
      }

      "not allow registered usernames" in new Setup {
         sedisMock.get(eqTo("applicationUser:username:popular:userId")) returns Some("123")
         sedisMock.get(eqTo("applicationUser:userId:123:email")) returns Some("john@example.com")
         sedisMock.get(eqTo("applicationUser:userId:123:fullname")) returns Some("john")

         applicationUserRepository.isUsernameAlreadyRegistered("popular") must beTrue
      }
   }

   "findByUsername" should {
      "find user" in new Setup {
         sedisMock.get(eqTo("applicationUser:username:popular:userId")) returns Some("123")
         sedisMock.get(eqTo("applicationUser:userId:123:email")) returns Some("john@example.com")
         sedisMock.get(eqTo("applicationUser:userId:123:fullname")) returns Some("john")

         applicationUserRepository.findByUsername("popular") must beEqualTo (Some(ApplicationUser(Some("123"), "popular", Some("john@example.com"), Some("john"))))

         there was three(sedisMock).get(anyString)
      }

      "not find user" in new Setup {
         sedisMock.get(anyString) returns None

         applicationUserRepository.findByUsername("popular") must be (None)

         there was one(sedisMock).get(anyString)
      }
   }
}
