package models

import org.specs2.specification.Scope
import org.specs2.mock._
import org.specs2.mutable._
import org.mockito.Matchers.{eq => eqTo}
import org.sedis.Pool
import redis.clients.jedis.{Jedis,JedisPool}
import scala.collection.JavaConversions._

class SearchSpec extends Specification with Mockito {

   trait Setup extends Scope {

      implicit val applicationUserRepositoryMock = mock[ApplicationUserRepository]
      implicit val partnerRepositoryMock = mock[PartnerRepository]
      implicit val childRepositoryMock  = mock[ChildRepository]
      implicit val whoRepositoryMock = mock[WhoRepository]
      implicit val sedisWrapperMock = mock[SedisWrapper]

      implicit val searchWho = new SearchWho {
         val whoRepository = whoRepositoryMock
         implicit val sedis = sedisWrapperMock
         val MaxWhosToFind = 10
      }

      val appUser =  ApplicationUser( Some("someUserId"), "username", Some("email"), Some("fullname"))
      val who1 = new Who(Some("1"),"who 1")
      val who2 = new Who(Some("2"),"who 2")
      val who3 = new Who(Some("3"),"who 3")
   }


  "search using find who box" should {

   "return all whos when search term is blank" in new Setup {
      whoRepositoryMock.findAllIds(any[ApplicationUser]) returns Set("1","2")
      whoRepositoryMock.findById(appUser,"1") returns Some(who1)
      whoRepositoryMock.findById(appUser,"2") returns Some(who2)

      val whos = searchWho.searchByPartialName( appUser, "" )

      whos must haveSize(2)
   }

   "return all who start with s when search for s" in new Setup {
      sedisWrapperMock.smembers(anyString) returns Set("1","2","3")
      whoRepositoryMock.findById(appUser,"1") returns Some(who1)
      whoRepositoryMock.findById(appUser,"2") returns Some(who2)
      whoRepositoryMock.findById(appUser,"3") returns Some(who3)

      val whos = searchWho.searchByPartialName( appUser, "s" )

      whos must haveSize(3)
   }

   "return all who start with s when search for capital S" in new Setup {
      sedisWrapperMock.smembers("search:userId:someUserId:search:name:s") returns Set("1","2","3")
      whoRepositoryMock.findById(appUser,"1") returns Some(who1)
      whoRepositoryMock.findById(appUser,"2") returns Some(who2)
      whoRepositoryMock.findById(appUser,"3") returns Some(who3)

      val whos = searchWho.searchByPartialName( appUser, "S" )

      whos must haveSize(3)
   }


   "return all who start with w t when search for capital w and t" in new Setup {
      sedisWrapperMock.smembers(s"search:userId:someUserId:search:name:w") returns Set("2")
      sedisWrapperMock.smembers(s"search:userId:someUserId:search:name:t") returns Set("2", "3")
      whoRepositoryMock.findById(appUser,"1") returns Some(who1)
      whoRepositoryMock.findById(appUser,"2") returns Some(who2)
      whoRepositoryMock.findById(appUser,"3") returns Some(who3)

      val whos = searchWho.searchByPartialName( appUser, "w t" )

      whos must haveSize(1)
      whos must contain(who2)
   }

   "return all who start with w t when search for capital t and w" in new Setup {
      sedisWrapperMock.smembers(s"search:userId:someUserId:search:name:w") returns Set("2")
      sedisWrapperMock.smembers(s"search:userId:someUserId:search:name:t") returns Set("2", "3")
      whoRepositoryMock.findById(appUser,"1") returns Some(who1)
      whoRepositoryMock.findById(appUser,"2") returns Some(who2)
      whoRepositoryMock.findById(appUser,"3") returns Some(who3)

      val whos = searchWho.searchByPartialName( appUser, "W T" )

      whos must haveSize(1)
      whos must contain(who2)
   }


   "return no whos if a new application user"  in new Setup {
      val appUser2 = ApplicationUser(Some("another id"), "username123t5", None, None)
      whoRepositoryMock.findAllIds(appUser2) returns Set.empty

      val whos = searchWho.searchByPartialName(appUser2, "" )

      whos must be empty
   }


   "return only my whos" in new Setup {
      val appUser2 = ApplicationUser(Some("another id"), "username123t5", None, None)
      whoRepositoryMock.findAllIds(appUser) returns Set("1", "2")
      whoRepositoryMock.findAllIds(appUser2) returns Set.empty
      whoRepositoryMock.findById(appUser,"1") returns Some(who1)
      whoRepositoryMock.findById(appUser,"2") returns Some(who2)

      val whos1 = searchWho.searchByPartialName(appUser, "" )
      val whos2 = searchWho.searchByPartialName(appUser2, "" )

      whos1 must haveSize(2)
      whos2 must be empty
   }

  }

}