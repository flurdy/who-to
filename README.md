Who To
=====================================

Of the people you are connected to, who are their immediate families?

What is the name of your friends' wifes, boyfriends, children, parents? How do you spell it again?

After awhile you start to forget and mix people's connections.

Other contact lists are for yours friends only, not their connection. With Facebook etc is up to your connections to add their relevant immediate connections and most don't.

This app is here to help you remember people's spouses', exes', etc names and never be embarrassed when meeting them again or writing birthday cards.

And only that. One purpose and do it well.


Live site
----
https://whoto.flurdy.io


Run this application
----

Check out the source: `git clone git@bitbucket.org:flurdy/who-to.git`

Install SBT: `brew install sbt`

Run : `sbt run`


Please contribute with suggestions, code, anything. http://bitbucket.org/flurdy/who-to
